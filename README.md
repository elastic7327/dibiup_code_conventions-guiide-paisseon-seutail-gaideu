![Screen Shot 2017-05-08 at 5.14.09 PM.png](https://bitbucket.org/repo/qEEGLeM/images/1902145081-Screen%20Shot%202017-05-08%20at%205.14.09%20PM.png)


![Screen Shot 2017-05-08 at 5.12.48 PM.png](https://bitbucket.org/repo/qEEGLeM/images/337125425-Screen%20Shot%202017-05-08%20at%205.12.48%20PM.png)


### 지속적으로 업데이트 중입니다 . . . . .
### v.1.0.1

### 가볍게 읽어주세요 하지만 자주 읽어주세요 
### 코드 리뷰의 가장 큰 기준이 됩니다.


이 문서는 코드컨벤션에 관한 글입니다. 

## 1 - 1 들여쓰기/함수(func) 
들여쓰기는 꼭 4칸을 기준으로합니다.
(Tab 과 스페이스4번은 엄연하게 다릅니다.
그리고 이 탭과 스페이스를 섞어서 쓰지마세요)


좋은 예 
```
#!python
Yes:


# Aligned with opening delimiter.
foo = func(var_one, var_two,
           var_three, var_four)

# 이 경우에는 라인넘버 때문에 세번째 변수를 선언하고 들여쓰기를 한 경우입니다 
# 깔끔하게 인자 부분의 들여쓰기가 맞아 떨어지기만 하면 나쁘지 않습니다
# More indentation included to distinguish this from the rest.
def func(
        var_one, var_two, var_three,
        var_four):
    print(var_one)

# 이런식으로 둘 둘 묶어서 사용 하여도 무방합니다. 2x2 이네요  
# 또 이경우에는 함수를 사용하는 경우이기 때문에 함수 들여쓰기 레벨과 인자 레벨을 
# 신경 쓰지 않았습니다. 무방합니다
# Hanging indents should add a level.
foo = func(
    var_one, var_two,
    var_three, var_four)

```

안좋은 예
```
#!python

No:

# 아래에 보시면 좋은 예와는 정반대로 인자부분의 들여쓰기가 잘 지켜지지 않았습니다
# 함수를 가져다 쓰는경우에는 함수 이름의 들여쓰기보다는, 인자들의 들여쓰기에 꼭
# 신경을 써주세요
# Arguments on first line forbidden when not using vertical alignment.
foo = func(var_one, var_two,
    var_three, var_four)

# 이 경우에는 위의 좋은 예시와 매우 흡사하지만 자세히 보시면,
# 이 코드는 함수를 정의 하는 코드임에도 불구하고, 들여쓰기를 지키지
# 않았습니다. 함수 이름과 인자의 들여쓰기 Level(레벨)이 같습니다. 엉망이군요
# Further indentation required as indentation is not distinguishable.
def func(
    var_one, var_two, var_three,
    var_four):
    print(var_one)
```


*
PyCharm 이나 Vim 또는 VScode , ATOM 등 모든 Editor에서는 파이선 indentation size 를 수정할 수 있습니다. 꼭 4칸으로 설정해주세요 
*

## 1 - 2 들여쓰기/리스트(list)


```
#!python

# 이런식으로 마지막 괄호를 닫는 부분이 ], }, ,) 왼쪽 끝(변수와 같은 들여쓰기 레벨)
# 으로 가는 것도 무방합니다 

my_list = [
    1, 2, 3,
    4, 5, 6,
    ]

# 위에 있었던 예시입니다. 인자와 같은 레벨의 들여쓰기를 한 예시입니다. 
# 무방합니다 
result = my_func(
    'a', 'b', 'c',
    'd', 'e', 'f',
    )

# 또는 위에 언급했던 , 인자의 들여쓰기 레벨과 동일하게 하는 예시를 확인해보시겠습니다.
or it may be lined up under the first character of the line that starts the multi-line construct, as in:

# 아래의 리스트를 보시면 괄호 닫는부분 "]" 이 인자의 들여쓰기 레벨과 같습니다
my_list = [
    1, 2, 3,
    4, 5, 6,
]

# 이 예시는 위에 이미 언급 했던 예시 입니다. 하지만 괄호 닫는 부분 ")" 이 
# 변수의 인자의 레벨과 같습니다. 
result = some_function_that_takes_arguments(
    'a', 'b', 'c',
    'd', 'e', 'f',
)

```


## 2 - 1  임포트(import)

좋은예
```
Yes:

#!python

# 임포트는 반드시 한라인 한라인 이렇게 나눠서 선언해야합니다.
# Imports should usually be on separate lines, e.g.:

import os
import sys
```
안좋은 예
```
No:  

#!python
# 이런식으로 쭉 , 늘어지게 하는것을 지양합니다. 
# 지양!합니다. 꼭 이것이 틀린 것은 아닙니다. 
import sys, os
```